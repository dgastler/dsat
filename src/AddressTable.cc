#include <AddressTable.hh>
#include <fstream>
#include <AddressTableException.hh>
#include <boost/tokenizer.hpp> //tokenizer
#include <stdlib.h>  //strtoul & getenv
#include <boost/regex.hpp> //regex
#include <boost/algorithm/string/case_conv.hpp> //to_upper


AddressTable::AddressTable(std::string const & addressTableName){
  fileLevel = 0;

  //Build the root device
  SubDevice * rootDevice = new SubDevice;
  rootDevice->device.assign("ROOT");
  rootDevice->deviceID = 0;
  rootDevice->address_range.push_back(std::pair<uint16_t,uint16_t>(0x0000,0xFFFF));
  deviceList.push_back(rootDevice);
  LoadFile(addressTableName,
	   "",
	   0,
	   rootDevice->deviceID);


  //DEBUG: display the devices found
//  for(size_t iDevice = 0; iDevice < deviceList.size();iDevice++){
//    fprintf(stderr,
//	    "\n%s\n\t%d\n\t%zu\n",
//	    deviceList[iDevice]->device.c_str(),
//	    deviceList[iDevice]->deviceID,
//	    deviceList[iDevice]->address_range.size()
//	    );
//    for(size_t i = 0; i < deviceList[iDevice]->address_range.size();i++){
//      fprintf(stderr,
//	      "\t\t%zu\t0x%04X 0x%04X\n",
//	      i,
//	      deviceList[iDevice]->address_range[i].first,
//	      deviceList[iDevice]->address_range[i].second);
//    }
//  }
  
  
}

AddressTable::~AddressTable(){
  //clean up items
  for(std::map<uint32_t,std::vector<Item*> >::iterator itMap = addressItemMap.begin();
      itMap != addressItemMap.end();
      itMap++){
    for(size_t iItem = 0; iItem < itMap->second.size();iItem++){
      delete itMap->second[iItem];
    }
  }

  for(size_t iDevice = 0; iDevice < deviceList.size();iDevice++){
    delete deviceList[iDevice];
  }


}
