SHELL = bash
PackagePath = $(shell pwd)

LIBRARY_ATABLE_SO_FILE = lib/libdsat.so
LIBRARY_ATABLE_SOURCES = $(wildcard src/*.cc)
LIBRARY_ATABLE_OBJECT_FILES = $(patsubst src/%.cc, obj/%.o, ${LIBRARY_ATABLE_SOURCES})
LIBRARY_BUEXCEPTION = /home/eason_yang/worke/submo_test/buexception/lib/libToolException.so

INCLUDE_PATH = \
				-Iinclude
LIBRARY_PATH = \
				-Lexternal/BUException/lib

CPP_FLAGS = -std=c++11 -g -O3 -rdynamic -Wall -MMD -MP -fPIC ${INCLUDE_PATH} -Werror -Wno-literal-suffix
CPP_FLAGS +=-fno-omit-frame-pointer -Wno-ignored-qualifiers -Werror=return-type -Wextra -Wno-long-long -Winit-self -Wno-unused-local-typedefs  -Woverloaded-virtual
LINK_LIBRARY_FLAGS = -Xlinker "--no-as-needed" -shared -fPIC -Wall -g -O3 -rdynamic ${LIBRARY_PATH} -Wl,-rpath=${PackagePath}/lib

defult: build
clean: _cleanall
_cleanall:
	rm -rf obj
	rm -rf lib

all: _all
build: _all
buildall: _all
_all: ${LIBRARY_ATABLE_SO_FILE}

${LIBRARY_ATABLE_SO_FILE}: ${LIBRARY_ATABLE_OBJECT_FILES} ${LIBRARY_BUEXCEPTION}
	g++ ${LINK_LIBRARY_FLAGS} ${LIBRARY_ATABLE_OBJECT_FILES} -lboost_regex -lToolException -o $@

obj/%.o : src/%.cc
	mkdir -p $(dir $@)
	mkdir -p {lib,obj}
	g++ ${CPP_FLAGS} -c $< -o $@

${LIBRARY_BUEXCEPTION} :
	${MAKE} -C external/BUException
