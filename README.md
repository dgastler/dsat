# DSAT
Dan's Simple Address Table (DSAT).
This is a library for a simple human readable address table format.
Example tables in ./test

#API
 - AddressTable(std::string const & addressTableName);
   Pass the constructor the top level address table file
   
 - std::vector<std::string>  GetNames    (std::string const & regex);
   Call to get the names of registers that follow the regex pattern

 - std::vector<std::string>  GetAddresses(uint16_t lower, uint16_t upper);
   Get the names of registers in the numerical range lower to uppper

 - Item const *              GetItem     (std::string const &);
   Get a const copy of the Item struct for the specified register name

 - Struct Item
   Not displaying the whole struct here, but inside of this you can get the
     - name
     - address
     - mask
     - mode
     - user data map

# TODO
 - This code doesn't have an example yet and one will be added soon (2021/10/25)
